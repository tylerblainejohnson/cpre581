\documentclass[12pt]{IEEEtran}
\usepackage{cite}
\usepackage[pdftex]{graphicx}
\usepackage[space]{grffile}
\usepackage{amsmath}
\graphicspath{{.}}
\DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\begin{document}
\date{December 30, 2012}
\title{Evaluation of the Cache Design Space with DineroIV}
\author{Tyler~Johnson, Joseph~Groenenboom}
\maketitle
\abstract
As the performance of modern processors continues to increase, memory latencies continue to become a larger portion of the required run time for a given application. One way to improve upon this access time is to use a cache to store data and instructions in a smaller, faster memory in order reduce the wait time for a memory access. This paper will focus on the cache design space and show the effects that certain design parameters have on the performance of a cache in terms of the hit and miss rates. The results will be generated by the DineroIV cache simulator platform, and the Spec CPU2006 benchmarks will be used as the memory access patterns. 
\section{Introduction}
\subsection{A Brief Background on Caches}
Caches have played a large role in computing since their introduction.  Caches have shorter access times than RAM or other long term storage mediums.  When data is accessed by a program it is traditionally brought into the CPU cache and operated on there to reduce the cost of redundant accesses by limiting accesses to slower forms of memory.  As such caches give the greatest performance increase when a program reuses data in the cache.\\*

Data reuse is defined by the type of locality that is expressed by the pattern of reuse.  There are two types of locality:\\*
\begin{itemize}
	\item Temporal Locality 
	\item Spacial Locality\\*
\end{itemize}

Temporal locality is the type of locality expressed by a program that uses a section of data over and over again.  The variable used to control iteration in for loops as seen in many programming languages is an expression of temporal locality.  Spatial locality, on the other hand, is seen when data values are accessed near each other in memory.  For example, iterating through an array of values sequentially is an example of perfect spatial locality. \\*

When designing a cache it is vital that these types of locality be leveraged to maximize data reuse in the cache.  The goal is to bring data into the cache and keep it there as long as it�s useful.  Data is replaced in a cache when the cache is full and new data needs to be brought in.  The mechanism that determines what data to replace is called a replacement strategy.  The replacement strategies offered by the dineroIV simulator follow:\\*
\begin{itemize}
	\item Least Recently Used (LRU)
	\item First In First Out (FIFO)
	\item Random \\*
\end{itemize}

LRU is used frequently and it favors temporal locality over spatial locality because it always selects the least recently used section of data in the cache to replace.  FIFO, on the other hand, always replaces that oldest piece of data in the cache regardless of it�s last access time.  Lastly random just selects a section to replace completely at random. \\*

In designing a cache there are several important features to consider.  The following is a list of some of those considerations:\\*
\begin{itemize}
	\item cache size
	\item associativity
	\item block size
	\item levels to the cache
	\item replacement schemes\\*
\end{itemize}

Cache size is important because ultimately it is what determines how much data can be held in the cache.  The associativity of the cache largely determines the level of temporal locality expressed by the cache.  A higher associativity allows data to stay in the cache longer because data is not evicted when a conflicting address from memory is loaded.  Block size is the feature of the cache that enhances spatial locality.  The size of a block is the amount of data that is taken from RAM when memory is copied into the cache.  The larger the block size the greater amount of consecutive values of copied from RAM.  The limiting factor for associativity and block size is the size of the cache.  The amount of space required for one entry in the cache is roughly equivalent to the block size multiplied by the associativity for one entry in the cache.  As such these two design aspects must be balanced when designing a cache.\\*

The last consideration that this paper examines for a cache design is the number of levels in a cache.  A cache can have different sizes and types of memory.  These caches are called multi-level caches and they are the standard for most CPU caches used today.  The lowest level of the cache is the fastest and smallest with speed decreasing and size increasing as the level of cache increases.  In a uniprocessor model the levels of the cache are simply additional units of memory.  The goal of a multi-level cache is to provide greater amounts of cache memory and further limit access to RAM.  Through the use of multi-level caches it is often possible to place most of the data needed to run a program in the cache on modern processors.

\subsection{Focus}
This paper will focus on the design space for a uniprocessor cache design. The focus will be specifically on the performance of data cache designs. The type of cache that will be evaluated is a design where the data and instruction caches are separate, which is considered a non-unified cache. This is a constraint from the simulator that will be used, and it is not a decision by the authors. There will be L1, L2, and L3 cache designs considered in the performance analysis. Along with the number of cache levels, the following design properties will also be evaluated:
\\*
\begin{itemize}
	\item Cache size (Data and Instruction)
	\item Block size (Data and Instruction)
	\item Replacement policy (Data and Instruction)
	\item Associativity (Data and Instruction)
	\\*
\end{itemize}

These cache properties will be simulated using the DineroIV cache simulator platform\cite{dineroIV}. The DineroIV simulator allows for up to five cache levels, but since currently technologies are capping out around three levels, the paper will only focus on three levels. The DineroIV simulator allows for the following design parameters for each level of the cache to be varied:
\\*
\begin{itemize}
  \item Cache size
  \item Block size
  \item Sub block size
  \item Associativity
  \item Replacement policy
  \item Fetch policy
  \item Prefetch distance
  \item Write allocate policy
  \item Write back policy
  \\*
\end{itemize}

This paper will make the following assumptions or generalizations to decrease the number of simulations required to collect data:
\\*
\begin{itemize}
	\item The data and instruction cache are independent
	\item The instruction cache will not have a write back policy
	\item The block size will be equivalent across all levels of the cache
	\item There is no prefetching done
	\item The sub block size will be the same as the block size
	\item The cache is non-unified
	\\*
\end{itemize}
\subsection{Benchmarks}
The paper will be using the a subset of benchmarks from the Spec CPU2006 benchmark suite. Five benchmarks were selected from both the integer and floating point categories in order to provide a balanced analysis. The benchmarks that have been selected are as follows:
\\*
\begin{itemize}
 \item Integer
 	\begin{itemize}
 		\item 401 Bzip2
		 \item 403 GCC
		 \item 462 Libquantum
		 \item 464 h264ref
		 \item 473 Astar
		 \\*
	 \end{itemize}
  \item Floating Point
	  \begin{itemize}
	 	 \item 410 Bwaves
		  \item 450 Soplex
		  \item 470 LBM
		  \item 481 WRF
		  \item 482 Sphinx3
		  \\*
	\end{itemize}
\end{itemize}	
\subsection{Building Memory Trace Files}
In order to run the simulations with DineroIV a memory trace file is needed in the .din format, a unique format for the DineroIV simulator. The .din format is as follows:
\begin{itemize}
	\item Instruction Reads
	\begin{itemize}
		\item 0 [Instruction Address]
	\end{itemize}
	\item Data Reads
	\begin{itemize}
		\item 1 [Data Address]
	\end{itemize}
	\item Data Writes
	\begin{itemize}
		\item 2 [Data Address] \\*
	\end{itemize}
\end{itemize}

To create the memory trace files several steps were required. The first step to this process was to write a simple program that pintools could use to gather the required information, that is the memory accesses in this case. 	Pintools\cite{pintools} has many examples included with the toolset. The pinatrace.cpp file that is included in the ManualExamples directory is a good starting point to gather the needed information. With some modifications this example can be modified to directly output the trace file in the .din format. Once this is written the program needs to be compiled.\\*

After the pintools program has been created the next step is to build the Spec CPU 2006 benchmark that is to be used. This can be done using the Spec tools that are included to build the benchmark. After the benchmark has been properly build, navigate to the directory that contains the run folder for the benchmark. Inside this directory issue the command: 
\begin{center}
\emph{specinvoke -n\\*}
\end{center}

This command will print out a list of commands that are used to run the respective benchmark. Some benchmarks will show more than one command because there are multiple runs for that benchmark. When this was the case, the first command from the list was selected. This still allowed for adequate sized trace files. 
\\*

After the benchmark has been build, everything is prepared to run. The next step is to run the benchmark with pintools attached to the process. This is done by launching pintools with the correct arguments An example of this could look like:
\\*

\emph{./pin -t [path to the trace file program] -- [command from specinvoke -n]}
\\*

After the  trace files were generated they were then  cut to the first 10 million lines. This was done in order to allow for a reasonable simulation time. The first 10 million lines was a decision made based on reproducibility, it is possible that they do not reflect the true behavior of the benchmark being represented, but it was a necessary step to allow for reasonable run times.

\section{Simulation Results}
\subsection{L1 Data Caches}

This section will review the designs with only the first level of cache. The sizes of the level 1 cache have been varied from 4 KB to 256 KB increasing by powers of two. The block sizes have been varied from 1 byte to 64 bytes (shown in bits on all figures) increasing by powers of two. The associativity has been varied from 1 to 8 in powers of two. First the smallest size of level 1 cache with the smallest associativity will be considered. Figure~\ref{fig:1} will show results for all block sizes that were simulated for this cache.\\*
\begin{figure}[h]
  \centering
    \includegraphics[width=0.5\textwidth]{L1 Graph Exports/l1_32768_datacachemissresults/l1_32768_datacachemissresults-1.png}
     \caption{Simulation Results for L1 cache of size 32768 bits and Associativty of 1} \label{fig:1}
\end{figure}

This cache design has an alarmingly high miss rate for some of the benchmarks. Benchmark 470 has close to a 100\% miss rate when the block size is 8 bits as seen in figure~\ref{fig:1}. It can be seen that as the block size increases the miss rate decreases in most cases. There are a few benchmarks that this claim does not hold valid. For example when the block size is 512 bits for 403, the miss rate actually increases. See figure~\ref{fig:1} for this example.\\*

\begin{figure}[h]
  \centering
    \includegraphics[width=0.5\textwidth]{L1 Graph Exports/l1_32768_datacachemissresults/l1_32768_datacachemissresults-4}
     \caption{Simulation Results for L1 cache of size 32768 bits and Associativty of 8} \label{fig:2}
\end{figure}

Figure~\ref{fig:2} shows the same sized cache with an associativity of 8. It can bee seen in figure~\ref{fig:2} that increasing the associativity helps to improve on the miss rates. Benchmark 410 has quite a large improvement on the miss rate. However, for this size of cache the associativity does not have dramatic impacts on the performance for most of these benchmarks. This suggests that efforts could probably be spend on other design areas with a cache of this size.\\*

Figure~\ref{fig:3} shows the largest size of first level cache that was simulated. 
\begin{figure}[h]
  \centering
    \includegraphics[width=0.5\textwidth]{L1 Graph Exports/l1_2097152_datacacheresults/l1_2097152_datacacheresults-1.png}
     \caption{Simulation Results for L1 cache of size 2097152 bits and Associativty of 1} \label{fig:3}
\end{figure}
This graph represents a design with an associativity of 1 and all block sizes that were considered. Again it can be seen that there are very high miss rates when this block size is small. The performance tends to improve as the block size increases again in this case. Notice that in figure~\ref{fig:3} increasing the block size always has a positive impact on the miss rate, as opposed to figure~\ref{fig:1}.\\*

To reflect a little more on the effects that the associativity and block size have on the designs for this size of cache figure~\ref{fig:4} will be introduced. This figure represents a simulation with the 401 benchmark exclusively, and for a cache size of 32768 bits. The associativity and block sizes are swapped from the previous figures that have been included.\\*\\*
\begin{figure}[h]
  \centering
    \includegraphics[width=0.5\textwidth]{misc graphs/32768associativity.png}
     \caption{Simulation Results for L1 cache of size 32768 bits for benchmark 401. A new format is presented to make improvements more obvious} \label{fig:4}
\end{figure}

With figure~\ref{fig:4} it is easier to see which of these two terms has the most impact on this cache design. With the block size of 1 byte the miss is by far the largest. Notice that changing the associativity here has little to no effect on the miss rate. As the block size increases the miss rates decrease. This trend starts to become less obvious around a block size of 16 bytes. 

\subsection{Metric for Multilevel Cache Miss Rate Analysis}

Before further evaluation is done on the level two and level three cache simulations, a new metric will be introduced. In the analysis of the level 1 caches, the miss rate is fully representative of the true miss rate for the level 1 cache. However, in a multilevel cache, the miss rate for a specific level do not reflect the true performance of the cache. For example consider a two level cache design. If there are 10,000 memory references and 4,000 misses in the level on cache the miss rate will be 4,000/10,000. Those misses will then propagate to the next level of cache, in this example that would be level 2. This means that level two only sees the 4,000 references that were missed in the level 1 cache. 
\\*

Now, suppose that the level 2 cache only contains half of the 4,000 references that were requested. That would indicate that the miss rate of the level 2 cache is 50\%. This 50\% miss rate is only true for the local misses, that is on the 4,000 instructions. This causes the higher levels in a cache to appear to have higher miss rates in some cases. See Table~\ref{tab:1} for this example.

\begin{table}[h]
\begin{center}
\begin{tabular}{|c|c|c|}
\hline \bfseries & \bfseries L1 & \bfseries L2 \\
\hline \bfseries References & \bfseries 10,000 & \bfseries 4,000\\
\hline \bfseries Misses & \bfseries 4,000 & \bfseries 2,000\\
\hline \bfseries Miss Rate & \bfseries 40\% & \bfseries 50\%\\
\hline
\end{tabular}
\end{center}
\caption{Local Miss Rate Example}
\label{tab:1}
\end{table}

In order to give a better representation of the overall performance, a different metric will be introduced. This new metric is called the global hit rate\cite{hennessy2011computer}. This global hit rate considers the miss rate for each level of cache with respect to the overall miss rate. This metric can be calculated by the following equation:

\begin{equation}
\begin{split}
Global Miss Rate L_n &= \\ &Local Miss Rate L_1 * Local Miss Rate L_2\\& *  ... *  Local Miss Rate L_n.
\end{split}
\end{equation}
\\*
Revisiting the previous example the miss rates would look like this:
\begin{table}[h]
\begin{center}
\begin{tabular}{|c|c|c|}
\hline \bfseries & \bfseries L1 & \bfseries L2 \\
\hline \bfseries References & \bfseries 10,000 & \bfseries 4,000\\
\hline \bfseries Misses & \bfseries 4,000 & \bfseries 2,000\\
\hline \bfseries Local Miss Rate & \bfseries 40\% & \bfseries 50\%\\
\hline \bfseries Global Miss Rate & \bfseries 40\% & \bfseries 20\%\\
\hline
\end{tabular}
\end{center}
\caption{Global Miss Rate Example}
\label{tab:2}
\end{table}

This new metric shows that even though the L2 cache was missing 50\% of the requested data, it is only actually missing 20\% of the time, since L2 is never accessed unless L1 misses. Note that for the first level of cache, the global and local miss rates are equal.
\\*

This metric will help to better understand the performance of the multilevel cache simulations that will be analyzed in the following two sections. The graphs included in the next two sections will contain the simulation results from a single Spec benchmark per graph. The horizontal axes will contain the associativity, the block size, and the level of cache. The vertical axes will only represent the global miss rates. 
\subsection{L1, L2 Data Caches}
This section will review the level two simulation results. The size of the level two caches that were simulated are 256 KB and 512 KB. By adding a second level cache the global miss rates of the cache are expected to decrease. This is due to the extra memory that the higher level provides. 
\\*

Figure~\ref{fig:5} shows a cache with a 4 KB level 1 and 256 KB level two. It can be seen that the miss rates in the L1 cache are relatively high, but at the L2 cache, the rates are much smaller. It can also be seen in figure~\ref{fig:5} that the block size is having a much larger impact on the miss rates than the associativity. This was also a trend in the previous section.
\begin{figure}[h]
  \centering
    \includegraphics[width=0.5\textwidth]{L2 Graph Exports/l1_I_Size_32768_l2_I_Size_2097152_l1_I_Repl_l_l3_I_Assoc_x/l1_I_Size_32768_l2_I_Size_2097152_l1_I_Repl_l_l3_I_Assoc_x-3.png}
     \caption{Cache with L1 = 4 KB and L2 = 256 KB } 
     \label{fig:5}
\end{figure}

In figure~\ref{fig:6}  a cache with L1 size of 16 KB and L2 size of 256 KB is represented. Here the L1 global miss rate has decreased in comparison the figure~\ref{fig:5}. This is due the larger L1 size. The L2 cache remains the same size as before, and the miss rates for the L2 are fairly similar to those in figure~\ref{fig:5}. Again the block size is having a more significant impact than the associativity is.

\begin{figure}[h]
  \centering
    \includegraphics[width=0.5\textwidth]{L2 Graph Exports/l1_I_Size_131072_l2_I_Size_2097152_l1_I_Repl_l_l3_I_Assoc_x/l1_I_Size_131072_l2_I_Size_2097152_l1_I_Repl_l_l3_I_Assoc_x-3.png}
     \caption{Cache with L1 = 16 KB and L2 = 256 KB } 
     \label{fig:6}
\end{figure}

In figure~\ref{fig:7} the L2 cache size has increased to 512 KB. The figure shows that for the block size of 1 byte, the performance is still low. However, as the block size increases the rates are much lower than in figures~\ref{fig:5} and~\ref{fig:6}.

\begin{figure}[h]
  \centering
    \includegraphics[width=0.5\textwidth]{L2 Graph Exports/l1_I_Size_131072_l2_I_Size_4194304_l1_I_Repl_l_l3_I_Assoc_x/l1_I_Size_131072_l2_I_Size_4194304_l1_I_Repl_l_l3_I_Assoc_x-3.png}
     \caption{Cache with L1 = 16 KB and L2 = 512 KB } 
     \label{fig:7}
\end{figure}
\clearpage
\subsection{L1, L2, L3 Data Caches}
Next third level caches were examined.  The sizes used for the third level caches were 1 MB and 2 MB.  Adding a third level of cache caused the global miss rates to decrease further due to the additional capacity of the third cache level.  Aside from the expected decrease to the global miss rate several benchmarks showed some atypical behavior.  For example consider figure~\ref{fig:8} and figure~\ref{fig:9} showing the simulation results for the 401 benchmark:
\begin{figure}[h]
  \centering
    \includegraphics[width=0.5\textwidth]{L3 Graph Exports/l1_I_Size_32768_l2_I_Size_2097152_l3_I_Size_8388608_l1_I_Repl_l_l3_I_Assoc/l1_I_Size_32768_l2_I_Size_2097152_l3_I_Size_8388608_l1_I_Repl_l_l3_I_Assoc-1.png}
     \caption{Cache with L1 = 4 KB, L2 = 256 KB and L3 = 1 MB} 
     \label{fig:8}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[width=0.5\textwidth]{L3 Graph Exports/l1_I_Size_2097152_l2_I_Size_4194304_l3_I_Size_16777216_l1_I_Repl_l_l3_I_Assoc/l1_I_Size_2097152_l2_I_Size_4194304_l3_I_Size_16777216_l1_I_Repl_l_l3_I_Assoc-1.png}
     \caption{Cache with L1 = 256 KB, L2 = 512 KB and L3 = 2 MB} 
     \label{fig:9}
\end{figure}

It can be seen in both figure~\ref{fig:8} and figure~\ref{fig:9} that the miss rates for all levels of the cache do not improve with the increase in cache size.  Additionally the miss rate was high at rates in the low 70�s.  This likely indicates that the cache was either not large enough or that the benchmark had very little data locality. That aside this benchmark again shows that block size has a dramatic impact on miss rate for the benchmarks used in this paper. The 482 benchmark showed typical behavior in the change in global miss rate with increase in cache size.

\begin{figure}[ht]
  \centering
    \includegraphics[width=0.5\textwidth]{L3 Graph Exports/l1_I_Size_32768_l2_I_Size_2097152_l3_I_Size_8388608_l1_I_Repl_l_l3_I_Assoc/l1_I_Size_32768_l2_I_Size_2097152_l3_I_Size_8388608_l1_I_Repl_l_l3_I_Assoc-10.png}
     \caption{Cache with L1 = 4 KB, L2 = 256 KB and L3 = 1 MB} 
     \label{fig:10}
\end{figure}

\begin{figure}[ht]
  \centering
    \includegraphics[width=0.5\textwidth]{L3 Graph Exports/l1_I_Size_2097152_l2_I_Size_4194304_l3_I_Size_16777216_l1_I_Repl_l_l3_I_Assoc/l1_I_Size_2097152_l2_I_Size_4194304_l3_I_Size_16777216_l1_I_Repl_l_l3_I_Assoc-10.png}
     \caption{Cache with L1 = 256 KB, L2 = 512 KB and L3 = 2 MB} 
     \label{fig:11}
\end{figure}

In figure~\ref{fig:10} and figure~\ref{fig:11} no large decrease in the global miss rate can be seen for most cases but the global miss rate for the L1 cache with associativity one in figure~\ref{fig:10} improves drastically with an increase in cache size. In general 482 has a low global miss rate. This likely indicates that benchmark 482 has a great deal of data locality.  A second example of a decrease in the global miss rate with increasing block sizes may be seen in figure~\ref{fig:12} and figure~\ref{fig:13}.

\begin{figure}[ht]
  \centering
    \includegraphics[width=0.5\textwidth]{L3 Graph Exports/l1_I_Size_32768_l2_I_Size_2097152_l3_I_Size_8388608_l1_I_Repl_l_l3_I_Assoc/l1_I_Size_32768_l2_I_Size_2097152_l3_I_Size_8388608_l1_I_Repl_l_l3_I_Assoc-4.png}
     \caption{Cache with L1 = 4 KB, L2 = 256 KB and L3 = 1 MB} 
     \label{fig:12}
\end{figure}

\begin{figure}[ht]
  \centering
    \includegraphics[width=0.5\textwidth]{L3 Graph Exports/l1_I_Size_2097152_l2_I_Size_4194304_l3_I_Size_16777216_l1_I_Repl_l_l3_I_Assoc/l1_I_Size_2097152_l2_I_Size_4194304_l3_I_Size_16777216_l1_I_Repl_l_l3_I_Assoc-4.png}
     \caption{Cache with L1 = 256 KB, L2 = 512 KB and L3 = 2 MB} 
     \label{fig:13}
\end{figure}

Figure~\ref{fig:12}shows that the global miss rate for the L1 cache for benchmark 450 increases with a large block size.  This is most likely due to capacity misses caused by using a small L1 cache with a large block size.  This is confirmed in figure~\ref{fig:13} because the global miss rate again goes down with increasing block size with a larger L1 cache.  It can be seen in all of these figures that adding a third layer to the cache will decrease the global miss rate for most benchmarks.
\clearpage
\bibliographystyle{plain}
\bibliography{581Report}
\end{document}