#include "CacheLevel.hpp"

// TODO:: make max/min_size non-static

Cache_Level::Cache_Level(int in_level)
{
  level = in_level;
  if(level == 1)
    {
      //std::cout << "Cache init" << endl;
      //level = in_level;
      isize = 0;
      dsize = 0;
      ibsize = 0;
      dbsize = 0;
      //isbsize = ibsize;
      //dsbsize = dbsize;
      iassoc = 1;
      dassoc = 1;
      irepl = 'l';
      drepl = 'f';
      ifetch = 'd';
      dfetch = 'd';
      dwalloc = 'a';
      dwback = 'a';
      //iwback = 'a';
      //iwalloc = 'a';
      //skipcount = 0;  
      //flushcount = 0;
      //maxcount = 0;
      //start_interval = 0;
      //informat = 'd';
      //on_trigger = "0x0";
      //off_trigger = "0x0";
      max_size = 512 * KBITS_TO_BYTES;
      min_size = 4 * KBITS_TO_BYTES;
    }
  else if(level == 2)
    {
      //std::cout << "Cache init" << endl;
      //level = in_level;
      isize = 0;
      dsize = 0;
      ibsize = 0;
      dbsize = 0;
      //isbsize = ibsize;
      //dsbsize = dbsize;
      iassoc = 1;
      dassoc = 1;
      irepl = 'l';
      drepl = 'f';
      ifetch = 'd';
      dfetch = 'd';
      dwalloc = 'a';
      dwback = 'a';
      max_size = 1024 * KBITS_TO_BYTES;
      min_size = 256 * KBITS_TO_BYTES;
    }
  else
    {
      //std::cout << "Cache init" << endl;
      //level = in_level;
      isize = 0;
      dsize = 0;
      ibsize = 0;
      dbsize = 0;
      //isbsize = ibsize;
      //dsbsize = dbsize;
      iassoc = 1;
      dassoc = 1;
      irepl = 'l';
      drepl = 'f';
      ifetch = 'd';
      dfetch = 'd';
      dwalloc = 'a';
      dwback = 'a';
      max_size = 4096 * KBITS_TO_BYTES;
      min_size = 1024 * KBITS_TO_BYTES;
    }
}

Cache_Level::~Cache_Level(){    /*std::cout << "Cache Destruct" << endl;*/}

std::string Cache_Level::command_line()
{
  //std::cout << "Start command line" << endl;
  output.str("");
  output << " -l";
  output << level;
  output << "-isize ";
  output << isize;
  
  output << " -l";
  output << level;
  output << "-dsize ";
  output << dsize;
  
  output << " -l";
  output << level;
  output << "-ibsize ";
  output << ibsize;
    
  output << " -l";
  output << level;
  output << "-dbsize ";
  output << dbsize;
  /*
    output << " -l";
    output << level;
    output << "-isbsize ";
    output << isbsize;

    output << " -l";
    output << level;
    output << "-dsbsize ";
    output << dsbsize;
  */
  output << " -l";
  output << level;
  output << "-iassoc ";
  output << iassoc;
  
  output << " -l";
  output << level;
  output << "-dassoc ";
  output << dassoc;

  output << " -l";
  output << level;
  output << "-irepl ";
  output << irepl;
  
  output << " -l";
  output << level;
  output << "-drepl ";
  output << drepl;
    
  output << " -l";
  output << level;
  output << "-ifetch ";
  output << ifetch;

  output << " -l";
  output << level;
  output << "-dfetch ";
  output << dfetch;

  output << " -l";
  output << level;
  output << "-dwalloc ";
  output << dwalloc;
  
  output << " -l";
  output << level;
  output << "-dwback ";
  output << dwback;   
    
  /*
    output << " -l";
    output << level;
    output << "-iwback ";
    output << iwback;   

    output << " -l";
    output << level;
    output << "-iwalloc ";
    output << iwalloc;   
  */

  //std::cout << output.str() << endl;
  //std::cout << "end command line" << endl;

  return output.str();
}

int Cache_Level::set_run(int in_isize, int in_dsize, int in_ibsize, int in_dbsize, int in_iassoc, int in_dassoc, char in_irepl, char in_drepl, char in_ifetch, char in_dfetch, char in_dwalloc, char in_dwback /*, char in_iwback, char in_iwalloc*/)
{
  //std::cout << "start set run" << endl;
  isize = in_isize;
  dsize = in_dsize;
  ibsize = in_ibsize;
  dbsize = in_dbsize;
  //isbsize = ibsize;// TODO: These never change
  //dsbsize = dbsize;// independently.
  iassoc = in_iassoc;
  dassoc = in_dassoc;
  irepl = in_irepl;
  drepl = in_drepl;
  ifetch = in_ifetch;
  dfetch = in_dfetch;
  dwalloc = in_dwalloc;
  dwback = in_dwback;
  //iwback = in_iwback;
  //iwalloc = in_iwalloc;
  
  //std::cout << "end set run" << endl;
  return 0;
}

int Cache_Level::all_runs(int in_ibsize, int in_iassoc, char in_irepl, char in_ifetch, char in_dfetch, char in_dwalloc, char in_dwback, std::vector<Cache_Level*> levels, int place, std::string beg, std::string end, int* current_runs)
{

  if(place > 0)
    {

      //Debug
      //std::cout << place << std::endl << std::endl;


      for(int i = min_size; i < max_size; i = i<<1)
	{
	  //l1.set_run(is, is, icbs, icbs, assocI, assocI, *replI, *replI, 'd', 'd', 'a', 'a');
	  set_run(i, i, in_ibsize, in_ibsize, in_iassoc, in_iassoc, in_irepl, in_irepl, in_ifetch, in_dfetch, in_dwalloc, in_dwback);

	  //Debug~ish
	  //int replace = place -1;

	  levels[place-1]->all_runs(in_ibsize, in_iassoc, in_irepl, in_ifetch, in_dfetch, in_dwalloc, in_dwback, levels, place-1, beg, end, current_runs);
	}

      //return 2;
    }
  else
    {


      //Debug
      //std::cout << place << std::endl << std::endl;


      for(int i = min_size; i < max_size; i = i<<1)
	{
	  set_run(i, i, in_ibsize, in_ibsize, in_iassoc, in_iassoc, in_irepl, in_irepl, in_ifetch, in_dfetch, in_dwalloc, in_dwback);
	  //print stuff out

	  std::stringstream to_run;
	  output.str("");

	  to_run << beg;
	  
	  for(std::vector<Cache_Level*>::iterator iter = levels.begin(); iter < levels.end(); iter++)
	    {
	      to_run << (*iter)->command_line();
	    }
	  
	  to_run << end;

	  (*current_runs)++;

	  //Debug -- uncomment for non-debug
	  system(to_run.str().c_str());
	  //Debug
	  //std::cout << to_run.str().c_str() << std::endl << std::endl;
	  //std::cout << "size of levels as seen in CacheLevel.cpp:: " << levels.size() << std::endl;

	  //return 1;
	}
    }

    //Debug
  //  std::cout << place << std::endl << std::endl;
  
  return 0;
}

int Cache_Level::count_runs(int in_ibsize, int in_iassoc, char in_irepl, char in_ifetch, char in_dfetch, char in_dwalloc, char in_dwback, std::vector<Cache_Level*> levels, int place, int *run_estimate)
{
  if(place > 0)
    {
      for(int i = min_size; i < max_size; i = i<<1)
	{
	  levels[place-1]->count_runs(in_ibsize, in_iassoc, in_irepl, in_ifetch, in_dfetch, in_dwalloc, in_dwback, levels, place-1, run_estimate);
	}
    }
  else
    {
      for(int i = min_size; i < max_size; i = i<<1)
	{
	  (*run_estimate)++;
	}
    }
  
  return 0;
}
