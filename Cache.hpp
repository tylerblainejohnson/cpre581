#ifndef CACHE
#define CACHE

//dependencies
#include "CacheLevel.hpp"
#include<vector>
#include<iostream>
#include<string>

class Cache
{
private:
  int level_count;
  std::vector<Cache_Level*> levels;
  std::vector<char> replacement;
  std::vector<char> fetch_policy;
  std::vector<char> write_allocate;
  std::vector<char> write_back;
  int run_estimate;
  int current_runs;
  std::string outfile;

public:
  Cache(int in_lvl_count, std::string in_outfile);
  virtual ~Cache();
  std::string one_run();
  int runs(std::string beg, std::string end);
  int num_runs();
};

#endif
