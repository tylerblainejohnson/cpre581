#include<iostream>
#include<syscall.h>
#include<vector>
#include<string>
#include<sstream>

#include "Cache.hpp"

using namespace std;

int main(int argc, char **argv)
{
  //cout << "start main" << endl;

  string trace_file;
  string file_out;
  string dinero;
  const string usage = "Usage: ./RUNS -t [trace file] -o [output file] -d [dinero w/ path] -l [0 < cache_levels <= 3]";

  int cache_levels = 0;

  int i = 1;

  while(argv[i] != NULL)
    {

      if(argv[i][1] == 't')
	{
	  trace_file = argv[i+1];

	  i += 2;
	}
      else if(argv[i][1] == 'o')
	{
	  file_out = argv[i+1];

	  i += 2;
	}
      else if(argv[i][1] == 'd')
	{
	  dinero = argv[i+1];

	  i += 2;
	}
      else if(argv[i][1] == 'l')
	{
	  cache_levels = (int) (argv[i+1][0] - '0');

	  i += 2;
	}
      else
	{
	  cout << "Incorrect input parameter" << endl << usage << endl;

	  return 1;
	}
    }

  if(trace_file.empty() || file_out.empty() || dinero.empty() || (cache_levels == 0) || (cache_levels > 3))
    {
      cout << "Incorrectly usage." << endl << usage << endl;
      return 2;
    }

  const char INFORMAT = 'd'; // Not changed w/o new trace files
  const int SKIPCOUNT = 0;
  const int FLUSHCOUNT = 0; 
  const int MAXCOUNT = 0; 
  const int STAT_INTERVAL = 0;
  const string ON_TRIGGER = "0x0"; 
  const string OFF_TRIGGER = "0x0";
  
  Cache my_cache(cache_levels, file_out);

  //Debug
  //cout << "my_cache.levels.size " << my_cache.levels.size() << endl;


  // The stream for a single run. 
  //stringstream one_run;
  stringstream begining;
  stringstream end;

  begining.str("");
  end.str("");

  // Tests to run:
  // -catche size balance
  // - oversize L1 cache
  //   - same for the others
  // - sprinkle in the fetch/write poilicy used


  //  int runs = 0;
  //  const int totoal_runs = (8*7*4*4*3*3); // not static so this is very temporary 
  //  int completeion = 0;

//   for(int is = 4 * (8*1024); is <= (512 * (8*1024)); is = (is<<1))
// 	//for(int ds = 4 * (8*1024); ds <= (512 * (8*1024)); ds = ((ds<<1)))
// 	for(int icbs = 8; icbs <= 512; icbs = icbs<<1)
// 	//for(int dcbs = 16; dcbs <= 256; dcbs = dcbs<<1)
// 	  for(int assocI = 1; assocI <= 8; assocI = assocI<<1)
// 	    //for(int assocD = 1; assocD <= 8; assocD = assocD<<1)
// 	  for(vector<char>::iterator replI = replacement.begin(); replI < replacement.end(); replI++)
// 	    //for(vector<char>::iterator replD = replacement.begin(); replD < replacement.end(); replD++)
// 	    for(vector<char>::iterator wbackD =  write_back.begin(); wbackD < write_back.end(); wbackD++)
// 	  //    for(vector<char>::iterator wbackI =  write_back.begin(); wbackI < write_back.end(); wbackI++)
// 		      {
// 			// the method sig.
// 			//   int set_run(int in_isize, int in_dsize, int in_ibsize, int in_dbsize, int in_iassoc, int in_dassoc, char in_irepl, char in_drepl, char in_ifetch, char in_dfetch, char in_dwalloc, char in_dwback /*, char in_iwback, char in_iwalloc*/)

// 			//l1.set_run(is, ds, icbs, dcbs, assocI, assocD, *replI, *replD, 'd', 'd', 'a', *wbackD /*, *wbackI, 'a'*/);
// 			l1.set_run(is, is, icbs, icbs, assocI, assocI, *replI, *replI, 'd', 'd', 'a', 'a');
			
// 			one_run.str("");
			
// 			//one_run << "../dineroIV ";
// 			one_run << dinero;
// 			one_run << " ";
// 			one_run << l1.command_line();
// 			//  one_run // TODO: the defaults
			
// 			// TODO:: Make me flexible
// 			one_run << " -skipcount " << SKIPCOUNT << " -flushcount " << FLUSHCOUNT << "  -maxcount " << MAXCOUNT << "  -stat-interval " << STAT_INTERVAL << " -informat " << INFORMAT <<" -on-trigger " << ON_TRIGGER << " -off-trigger " << OFF_TRIGGER;
			
// 			//one_run << " < cc1.din >> " << file_out;
// 			one_run << " < " << trace_file << " >> " << file_out;
			
// 			//TODO:: Output
// 			//cout << one_run.str() << endl;
			
// 			system(one_run.str().c_str());

// 			if(runs == 403)
// 			  {
// 			    completeion += 5;
// 			    runs = 0;
// 			    cout << completeion << "%" << endl;
// 			    //return 40;
// 			  }  
// 			runs++;

			
// 		      }

  //cout << "end main" << endl;

  begining << dinero;
  begining << " ";

  end << " -skipcount " << SKIPCOUNT << " -flushcount " << FLUSHCOUNT << "  -maxcount " << MAXCOUNT << "  -stat-interval " << STAT_INTERVAL << " -informat " << INFORMAT <<" -on-trigger " << ON_TRIGGER << " -off-trigger " << OFF_TRIGGER;
  end << " < " << trace_file << " >> " << file_out;

  my_cache.runs(begining.str(), end.str());
  //my_cache.num_runs();


  return 0;
}
