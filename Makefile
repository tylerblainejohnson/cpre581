all: RUNS

debug: DEBUG_RUNS

CacheLevel.o: CacheLevel.cpp CacheLevel.hpp
	g++ -c CacheLevel.cpp

Cache.o: CacheLevel.hpp Cache.cpp Cache.hpp
	g++ -c Cache.cpp

runs.o: runs.cpp Cache.hpp
	g++ -c runs.cpp

RUNS: runs.o CacheLevel.o Cache.o
	g++ -o RUNS runs.o CacheLevel.o Cache.o

DEBUG_CacheLevel.o: CacheLevel.cpp CacheLevel.hpp
	g++ -c -g CacheLevel.cpp -o DEBUG_CacheLevel.o

DEBUG_Cache.o: CacheLevel.hpp Cache.cpp Cache.hpp
	g++ -c -g Cache.cpp -o DEBUG_Cache.o

DEBUG_runs.o: runs.cpp Cache.hpp
	g++ -c -g runs.cpp -o DEBUG_runs.o

DEBUG_RUNS: DEBUG_runs.o DEBUG_CacheLevel.o DEBUG_Cache.o
	g++ -o DEBUG_RUNS -g DEBUG_runs.o DEBUG_CacheLevel.o DEBUG_Cache.o

clean:
	rm RUNS CacheLevel.o runs.o Cache.o

real_clean:
	rm DEBUG_RUNS DEBUG_CacheLevel.o DEBUG_runs.o DEBUG_Cache.o
	rm RUNS CacheLevel.o runs.o Cache.o