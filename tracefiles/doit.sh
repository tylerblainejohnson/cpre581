#! /bin/bash

if [ -e "runResults" ]
then
    backupExtension=`date +"%m_%d_%y_%T"`
    newFolder="runResults.Bak."
    newFolder=$newFolder$backupExtension
    newFolder=$newFolder"/"
    echo "Moving runResults folder to runResults.bak.$backupExtension"
    mv runResults/ $newFolder
    mkdir runResults
fi

if [ ! -e "runResults" ]
then
    mkdir runResults
fi

echo "starting bzip run"
#bzip
time ../RUNS -t 401bzip2.din -o runResults/401Out.txt -d ../../d4-7/dineroIV &
#403gcc.din
echo "starting gcc run"
time ../RUNS -t 403gcc.din -o runResults/403gcc.din -d ../../d4-7/dineroIV &
#410bwaves.din
echo "starting bwaves run"
time ../RUNS -t 410bwaves.din -o runResults/410bwaves.din -d ../../d4-7/dineroIV &
#450soplex.din
echo "starting soplex run"
time ../RUNS -t 450soplex.din -o runResults/450soplex.din -d ../../d4-7/dineroIV &
#462libquantum.din
echo "starting libquantum run"
time ../RUNS -t 462libquantum.din -o runResults/462libquantum.din -d ../../d4-7/dineroIV &
#464h264ref.din
echo "starting h264ref run"
time ../RUNS -t 464h264ref.din -o runResults/464h264ref.din -d ../../d4-7/dineroIV &
#470lbm.din
echo "starting lbm run"
time ../RUNS -t 470lbm.din -o runResults/470lbm.din -d ../../d4-7/dineroIV &
#473astar.din
echo "starting astar run"
time ../RUNS -t 473astar.din -o runResults/473astar.din -d ../../d4-7/dineroIV &
#481wrf.din
echo "starting wlf run"
time ../RUNS -t 481wrf.din -o runResults/481wrf.din -d ../../d4-7/dineroIV &
#482sphinx3.din
echo "starting sphinx3 run"
time ../RUNS -t 482sphinx3.din -o runResults/482sphinx3.din -d ../../d4-7/dineroIV &
