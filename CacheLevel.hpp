#ifndef CACHE_LEVEL
#define CACHE_LEVEL

#define KBITS_TO_BYTES 8*1024

// dependencies 
#include<string>
#include<sstream>
#include<iostream>
#include<vector>

class Cache_Level
{
private:
  int level;
  std::stringstream output;

  int max_size;
  int min_size;

  int isize;
  int dsize;
  int ibsize;
  int dbsize;
  //int isbsize;
  //int dsbsize;
  int iassoc;
  int dassoc;
  char irepl;
  char drepl;
  char ifetch;
  char dfetch;
  char dwalloc;
  char dwback;
  //char iwback;
  //char iwalloc;
  // TODO: iwalloc, iwback
  //int shipcount;
  //int flushcount;
  //int maxcount;
  //int start_interval;
  //char informat;
  //std::string on_trigger;
  //std::string off_trigger;
public:
  Cache_Level(int in_level);

  virtual ~Cache_Level();

  std::string command_line();
  
  int set_run(int in_isize, int in_dsize, int in_ibsize, int in_dbsize, int in_iassoc, int in_dassoc, char in_irepl, char in_drepl, char in_ifetch, char in_dfetch, char in_dwalloc, char in_dwback /*, char in_iwback, char in_iwalloc*/);
  
  int all_runs(int in_ibsize, int in_iassoc, char in_irepl, char in_ifetch, char in_dfetch, char in_dwalloc, char in_dwback, std::vector<Cache_Level*> levels, int place, std::string beg, std::string end, int* current_runs);

  int count_runs(int in_ibsize, int in_iassoc, char in_irepl, char in_ifetch, char in_dfetch, char in_dwalloc, char in_dwback, std::vector<Cache_Level*> levels, int place, int *run_estimate);
};

#endif
