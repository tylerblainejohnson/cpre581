#include "Cache.hpp"

Cache::Cache(int in_lvl_count, std::string in_outfile)
{
  replacement.push_back('l'); // LRU
  replacement.push_back('f'); // FIFO
  replacement.push_back('r'); // Random

  fetch_policy.push_back('d'); //Demand
  fetch_policy.push_back('a'); //Always
  fetch_policy.push_back('m'); //Miss
  fetch_policy.push_back('t'); //Tagged
  fetch_policy.push_back('l'); //Load Forward
  fetch_policy.push_back('s'); //Sub Block

  write_allocate.push_back('a'); // Always
  write_allocate.push_back('n'); // Never
  write_allocate.push_back('f'); // No Fetch

  write_back.push_back('a'); // Always
  write_back.push_back('n'); // Never
  write_back.push_back('f'); // No Fetch

  level_count = in_lvl_count;
  
  for(int i = 0; i < level_count; i++)
    {
      levels.push_back(new Cache_Level(i+1));
    }  

  run_estimate = 0;
  current_runs = 0;
  outfile = in_outfile;
}

Cache::~Cache()
{
  //Debug
  //std::cout << "Cache deleted." << std::endl;

  for(std::vector<Cache_Level*>::iterator iter = levels.begin(); iter < levels.end(); iter++)
    {
      delete *iter;
    }

}

int Cache::runs(std::string beg, std::string end)
{
  // int Cache_Level::all_runs(int in_isize, int in_ibsize, int in_iassoc, char in_irepl, char in_ifetch, char in_dfetch, char in_dwalloc, char in_dwback, std::vector<Cache_Level*> levels, int place)

  num_runs();

  for(int assocI = 1; assocI <= 8; assocI = assocI<<1)
    for(int icbs = 8; icbs <= 512; icbs = icbs<<1)
      for(std::vector<char>::iterator replI = replacement.begin(); replI < replacement.end(); replI++)
	//for(std::vector<char>::iterator wbackD =  write_back.begin(); wbackD < write_back.end(); wbackD++)
	{
	  //int Cache_Level::all_runs(int in_ibsize, int in_iassoc, char in_irepl, char in_ifetch, char in_dfetch, char in_dwalloc, char in_dwback, std::vector<Cache_Level*> levels, int place, std::string beg, std::string end)

	  levels[levels.size()-1]->all_runs(icbs, assocI, *replI, 'd', 'd', 'a', 'a',  levels, levels.size()-1, beg, end, &current_runs);

	  // To give % completion
	  std::cout << outfile << " " << (current_runs * 100) / (run_estimate) << "%" << std::endl;
	}

  return 0;
}

int Cache::num_runs()
{
  for(int assocI = 1; assocI <= 8; assocI = assocI<<1)
    for(int icbs = 8; icbs <= 512; icbs = icbs<<1)
      for(std::vector<char>::iterator replI = replacement.begin(); replI < replacement.end(); replI++)
	{
	  levels[levels.size()-1]->count_runs(icbs, assocI, *replI, 'd', 'd', 'a', 'a',  levels, levels.size()-1, &run_estimate);
	}

  //Debug
  //std::cout << "Runs: " << run_estimate << std::endl;

  return run_estimate;
}
