package sql;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import containers.simulationContainer;

public class SQLProject {
	Connection conn = null;
	Statement st = null;
	ResultSet rs = null;
	private String specbenchmark = "";
	static String url = "jdbc:mysql://localhost:3306/CPRE581";
	static String user = "root";
	static String password = "";
	static String insertSimCommand = "INSERT INTO sim_results(simIndex,l1_I_Size,l2_I_Size,l3_I_Size," +
			"l1_D_Size,l2_D_Size,l3_D_Size,l1_I_BSize,l2_I_BSize,l3_I_BSize," +
			"l1_D_BSize,l2_D_BSize,l3_D_BSize,l1_I_Assoc,l2_I_Assoc,l3_I_Assoc," +
			"l1_D_Assoc,l2_D_Assoc,l3_D_Assoc,l1_I_Repl,l2_I_Repl,l3_I_Repl,l1_D_Repl," +
			"l2_D_Repl,l3_D_Repl,l1_I_Fetch,l2_I_Fetch,l3_I_Fetch,l1_D_Fetch,l2_D_Fetch," +
			"l3_D_Fetch,l1_D_WAlloc,l2_D_WAlloc,l3_D_WAlloc,l1_D_WBack,l2_D_WBack,l3_D_WBack," +
			"l1_I_Demand_Fetches,l2_I_Demand_Fetches,l3_I_Demand_Fetches,l1_D_Demand_Fetches," +
			"l2_D_Demand_Fetches,l3_D_Demand_Fetches,l1_I_Demand_Misses,l2_I_Demand_Misses," +
			"l3_I_Demand_Misses,l1_D_Demand_Misses,l2_D_Demand_Misses,l3_D_Demand_Misses," +
			"l1_I_Demand_Miss_Rate,l2_I_Demand_Miss_Rate,l3_I_Demand_Miss_Rate," +
			"l1_D_Demand_Miss_Rate,l2_D_Demand_Miss_Rate,l3_D_Demand_Miss_Rate," +
			"l1_I_Bytes_From_Mem,l2_I_Bytes_From_Mem,l3_I_Bytes_From_Mem," +
			"l1_D_Bytes_From_Mem,l2_D_Bytes_From_Mem,l3_D_Bytes_From_Mem," +
			"l1_I_Bytes_To_Mem,l2_I_Bytes_To_Mem,l3_I_Bytes_To_Mem," +
			"l1_D_Bytes_To_Mem,l2_D_Bytes_To_Mem,l3_D_Bytes_To_Mem," +
			"l1_I_Total_Bytes,l2_I_Total_Bytes,l3_I_Total_Bytes," +
			"l1_D_Total_Bytes,l2_D_Total_Bytes,l3_D_Total_Bytes,specbenchmark) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?" +
			", ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?" +
			", ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?" +
			", ?, ?, ?, ?)";
	public SQLProject(String specbenchmark) {
		this.specbenchmark = specbenchmark;
		try{
			conn = DriverManager.getConnection(url,user,password);
			st = conn.createStatement();
            //rs = st.executeQuery("SELECT VERSION()");            
		}catch (SQLException ex) {
            Logger lgr = Logger.getLogger(SQLProject.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException ex1) {
                lgr = Logger.getLogger(SQLProject.class.getName());
                lgr.log(Level.WARNING, ex1.getMessage(), ex1);
            }
        }
	}	
	
	public void saveSimResults(simulationContainer results){
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(insertSimCommand);                           
			pst.setInt(1, 0);
			
			pst.setInt(2, results.l1_I_size);
			pst.setInt(3, results.l2_I_size);
			pst.setInt(4, results.l3_I_size);
			pst.setInt(5, results.l1_D_size);
			pst.setInt(6, results.l2_D_size);
			pst.setInt(7, results.l3_D_size);
			
			pst.setInt(8, results.l1_I_Bsize);
			pst.setInt(9, results.l2_I_Bsize);
			pst.setInt(10, results.l3_I_Bsize);
			pst.setInt(11, results.l1_D_Bsize);
			pst.setInt(12, results.l2_D_Bsize);
			pst.setInt(13, results.l3_D_Bsize);
			
			pst.setString(14, String.valueOf(results.l1_I_Assoc));
			pst.setString(15, String.valueOf(results.l2_I_Assoc));
			pst.setString(16, String.valueOf(results.l3_I_Assoc));
			pst.setString(17, String.valueOf(results.l1_D_Assoc));
			pst.setString(18, String.valueOf(results.l3_D_Assoc));
			pst.setString(19, String.valueOf(results.l1_D_Assoc));
			
			pst.setString(20, String.valueOf(results.l1_I_Repl));
			pst.setString(21, String.valueOf(results.l2_I_Repl));
			pst.setString(22, String.valueOf(results.l3_I_Repl));
			pst.setString(23, String.valueOf(results.l1_D_Repl));
			pst.setString(24, String.valueOf(results.l3_D_Repl));
			pst.setString(25, String.valueOf(results.l1_D_Repl));
			
			pst.setString(26, String.valueOf(results.l1_I_Fetch));
			pst.setString(27, String.valueOf(results.l2_I_Fetch));
			pst.setString(28, String.valueOf(results.l3_I_Fetch));
			pst.setString(29, String.valueOf(results.l1_D_Fetch));
			pst.setString(30, String.valueOf(results.l3_D_Fetch));
			pst.setString(31, String.valueOf(results.l1_D_Fetch));
			
			pst.setString(32, String.valueOf(results.l1_D_WAlloc));
			pst.setString(33, String.valueOf(results.l3_D_WAlloc));
			pst.setString(34, String.valueOf(results.l1_D_WAlloc));
			
			pst.setString(35, String.valueOf(results.l1_D_WBack));
			pst.setString(36, String.valueOf(results.l3_D_WBack));
			pst.setString(37, String.valueOf(results.l1_D_WBack));
			
			//l1_I_Demand_Fetches
			pst.setInt(38, results.l1_I_Demand_Fetches);
			pst.setInt(39, results.l2_I_Demand_Fetches);
			pst.setInt(40, results.l3_I_Demand_Fetches);
			pst.setInt(41, results.l1_D_Demand_Fetches);
			pst.setInt(42, results.l2_D_Demand_Fetches);
			pst.setInt(43, results.l3_D_Demand_Fetches);
			
			pst.setInt(44, results.l1_I_Demand_Misses);
			pst.setInt(45, results.l2_I_Demand_Misses);
			pst.setInt(46, results.l3_I_Demand_Misses);
			pst.setInt(47, results.l1_D_Demand_Misses);
			pst.setInt(48, results.l2_D_Demand_Misses);
			pst.setInt(49, results.l3_D_Demand_Misses);
			
			//l1_I_Demand_Miss_Rate
			pst.setDouble(50, results.l1_I_Demand_Miss_Rate);
			pst.setDouble(51, results.l2_I_Demand_Miss_Rate);
			pst.setDouble(52, results.l3_I_Demand_Miss_Rate);
			pst.setDouble(53, results.l1_D_Demand_Miss_Rate);
			pst.setDouble(54, results.l2_D_Demand_Miss_Rate);
			pst.setDouble(55, results.l3_D_Demand_Miss_Rate);
			
			//l1_I_Bytes_From_Mem
			pst.setInt(56, results.l1_I_Bytes_From_Mem);
			pst.setInt(57, results.l2_I_Bytes_From_Mem);
			pst.setInt(58, results.l3_I_Bytes_From_Mem);
			pst.setInt(59, results.l1_D_Bytes_From_Mem);
			pst.setInt(60, results.l2_D_Bytes_From_Mem);
			pst.setInt(61, results.l3_D_Bytes_From_Mem);
			
			pst.setInt(62, results.l1_I_Bytes_To_Mem);
			pst.setInt(63, results.l2_I_Bytes_To_Mem);
			pst.setInt(64, results.l3_I_Bytes_To_Mem);
			pst.setInt(65, results.l1_D_Bytes_To_Mem);
			pst.setInt(66, results.l2_D_Bytes_To_Mem);
			pst.setInt(67, results.l3_D_Bytes_To_Mem);
			
			//l1_I_Total_Bytes
			pst.setInt(68, results.l1_I_Total_Bytes);
			pst.setInt(69, results.l2_I_Total_Bytes);
			pst.setInt(70, results.l3_I_Total_Bytes);
			pst.setInt(71, results.l1_D_Total_Bytes);
			pst.setInt(72, results.l2_D_Total_Bytes);
			pst.setInt(73, results.l3_D_Total_Bytes);
			
			pst.setString(74,this.specbenchmark);
			
			pst.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void close(){
		try {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(SQLProject.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
	}

}
