package containers;

public class simulationContainer {
		//I Cache size strings
		public int l1_I_size;
		public int l2_I_size;
		public int l3_I_size;
		//D Cache size strings
		public int l1_D_size;
		public int l2_D_size;
		public int l3_D_size;
		//I cache block sizes
		public int l1_I_Bsize;
		public int l2_I_Bsize;
		public int l3_I_Bsize;
		//D cache block sizes
		public int l1_D_Bsize;
		public int l2_D_Bsize;
		public int l3_D_Bsize;
		//I cache associativity
		public String l1_I_Assoc;
		public String l2_I_Assoc;
		public String l3_I_Assoc;
		//D cache associativity
		public String l1_D_Assoc;
		public String l2_D_Assoc;
		public String l3_D_Assoc;
		//I cache replacement
		public String l1_I_Repl;
		public String l2_I_Repl;
		public String l3_I_Repl;
		//D cache replacement
		public String l1_D_Repl;
		public String l2_D_Repl;
		public String l3_D_Repl;
		//I fetch 
		public String l1_I_Fetch;
		public String l2_I_Fetch;
		public String l3_I_Fetch;
		//D fetch 
		public String l1_D_Fetch;
		public String l2_D_Fetch;
		public String l3_D_Fetch;
		//D write allocate
		public String l1_D_WAlloc;
		public String l2_D_WAlloc;
		public String l3_D_WAlloc; 
		//D write back
		public String l1_D_WBack;
		public String l2_D_WBack;
		public String l3_D_WBack;
		
		public int l1_I_Demand_Fetches;
		public int l2_I_Demand_Fetches;
		public int l3_I_Demand_Fetches;
		public int l1_D_Demand_Fetches;
		public int l2_D_Demand_Fetches;
		public int l3_D_Demand_Fetches;
		
		public int l1_I_Demand_Misses;
		public int l2_I_Demand_Misses;
		public int l3_I_Demand_Misses;
		public int l1_D_Demand_Misses;
		public int l2_D_Demand_Misses;
		public int l3_D_Demand_Misses;
		
		public double l1_I_Demand_Miss_Rate;
		public double l2_I_Demand_Miss_Rate;
		public double l3_I_Demand_Miss_Rate;
		public double l1_D_Demand_Miss_Rate;
		public double l2_D_Demand_Miss_Rate;
		public double l3_D_Demand_Miss_Rate;
		
		public int l1_I_Bytes_From_Mem;
		public int l2_I_Bytes_From_Mem;
		public int l3_I_Bytes_From_Mem;
		public int l1_D_Bytes_From_Mem;
		public int l2_D_Bytes_From_Mem;
		public int l3_D_Bytes_From_Mem;
		
		public int l1_I_Bytes_To_Mem;
		public int l2_I_Bytes_To_Mem;
		public int l3_I_Bytes_To_Mem;
		public int l1_D_Bytes_To_Mem;
		public int l2_D_Bytes_To_Mem;
		public int l3_D_Bytes_To_Mem;

		public int l1_I_Total_Bytes;
		public int l2_I_Total_Bytes;
		public int l3_I_Total_Bytes;
		public int l1_D_Total_Bytes;
		public int l2_D_Total_Bytes;
		public int l3_D_Total_Bytes;
		
		
		
		public simulationContainer(){
			//I Cache size strings
			l1_I_size = -1;
			l2_I_size = -1;
			l3_I_size = -1;
			//D Cache size strings
			l1_D_size = -1;
			l2_D_size = -1;
			l3_D_size = -1;
			//I cache block sizes
			l1_I_Bsize = -1;
			l2_I_Bsize = -1;
			l3_I_Bsize = -1;
			//D cache block sizes
			l1_D_Bsize = -1;
			l2_D_Bsize = -1;
			l3_D_Bsize = -1;
			//I cache associativity
			l1_I_Assoc = "x";
			l2_I_Assoc = "x";
			l3_I_Assoc = "x";
			//D cache associativity
			l1_D_Assoc = "x";
			l2_D_Assoc = "x";
			l3_D_Assoc = "x";
			//I cache replacement
			l1_I_Repl = "x";
			l2_I_Repl = "x";
			l3_I_Repl = "x";
			//D cache replacement
			l1_D_Repl = "x";
			l2_D_Repl = "x";
			l3_D_Repl = "x";
			//I fetch 
			l1_I_Fetch = "x";
			l2_I_Fetch = "x";
			l3_I_Fetch = "x";
			//D fetch 
			l1_D_Fetch = "x";
			l2_D_Fetch = "x";
			l3_D_Fetch = "x";
			//D write allocate
			l1_D_WAlloc = "x";
			l2_D_WAlloc = "x";
			l3_D_WAlloc = "x";
			//D write back
			l1_D_WBack = "x";
			l2_D_WBack = "x";
			l3_D_WBack = "x";
			 
			l1_I_Demand_Fetches = -1;
			l2_I_Demand_Fetches = -1;
			l3_I_Demand_Fetches = -1;
			l1_D_Demand_Fetches = -1;
			l2_D_Demand_Fetches = -1;
			l3_D_Demand_Fetches = -1;
				
			l1_I_Demand_Misses = -1;
			l2_I_Demand_Misses = -1;
			l3_I_Demand_Misses = -1;
			l1_D_Demand_Misses = -1;
			l2_D_Demand_Misses = -1;
			l3_D_Demand_Misses = -1;
				
			l1_I_Demand_Miss_Rate = -1;
			l2_I_Demand_Miss_Rate = -1;
			l3_I_Demand_Miss_Rate = -1;
			l1_D_Demand_Miss_Rate = -1;
			l2_D_Demand_Miss_Rate = -1;
			l3_D_Demand_Miss_Rate = -1;
				
			l1_I_Bytes_From_Mem = -1;
			l2_I_Bytes_From_Mem = -1;
			l3_I_Bytes_From_Mem = -1;
			l1_D_Bytes_From_Mem = -1;
			l2_D_Bytes_From_Mem = -1;
			l3_D_Bytes_From_Mem = -1;
			
			l1_I_Bytes_To_Mem = -1;
			l2_I_Bytes_To_Mem = -1;
			l3_I_Bytes_To_Mem = -1;
			l1_D_Bytes_To_Mem = -1;
			l2_D_Bytes_To_Mem = -1;
			l3_D_Bytes_To_Mem = -1;

			l1_I_Total_Bytes = -1;
			l2_I_Total_Bytes = -1;
			l3_I_Total_Bytes = -1;
			l1_D_Total_Bytes = -1;
			l2_D_Total_Bytes = -1;
			l3_D_Total_Bytes = -1;
		}
		public void printSimulation(){
			System.out.println("Start Of Sim Data");
			
			System.out.println("l1_I_size");
			System.out.println(l1_I_size);
			
			System.out.println("l2_I_size");
			System.out.println(l2_I_size);
			
			System.out.println("l3_I_size");
			System.out.println(l3_I_size);
			
			System.out.println("l1_D_size");
			System.out.println(l1_D_size);
			
			System.out.println("l2_D_size");
			System.out.println(l2_D_size);
			
			System.out.println("l3_D_size");
			System.out.println(l3_D_size);

			System.out.println("l1_I_Bsize");
			System.out.println(l1_I_Bsize);
			
			System.out.println("l2_I_Bsize");
			System.out.println(l2_I_Bsize);
			
			System.out.println("l3_I_Bsize");
			System.out.println(l3_I_Bsize);
			
			System.out.println("l1_D_Bsize");
			System.out.println(l1_D_Bsize);
			
			System.out.println("l2_D_Bsize");
			System.out.println(l2_D_Bsize);
			
			System.out.println("l3_D_Bsize");
			System.out.println(l3_D_Bsize);
			
			System.out.println("l1_I_Assoc");
			System.out.println(l1_I_Assoc);
			
			System.out.println("l2_I_Assoc");
			System.out.println(l2_I_Assoc);
			
			System.out.println("l3_I_Assoc");
			System.out.println(l3_I_Assoc);
			
			System.out.println("l1_D_Assoc");
			System.out.println(l1_D_Assoc);
			
			System.out.println("l2_D_Assoc");
			System.out.println(l2_D_Assoc);
			
			System.out.println("l3_D_Assoc");
			System.out.println(l3_D_Assoc);
			
			System.out.println("l1_I_Repl");
			System.out.println(l1_I_Repl);
			
			System.out.println("l2_I_Repl");
			System.out.println(l2_I_Repl);
			
			System.out.println("l3_I_Repl");
			System.out.println(l3_I_Repl);
			
			System.out.println("l1_D_Repl");
			System.out.println(l1_D_Repl);
			
			System.out.println("l2_D_Repl");
			System.out.println(l2_D_Repl);
			
			System.out.println("l3_D_Repl");
			System.out.println(l3_D_Repl);
			
			System.out.println("l1_I_Fetch");
			System.out.println(l1_I_Fetch);
			
			System.out.println("l2_I_Fetch");
			System.out.println(l2_I_Fetch);
			
			System.out.println("l3_I_Fetch");
			System.out.println(l3_I_Fetch);
			
			System.out.println("l1_D_Fetch");
			System.out.println(l1_D_Fetch);
			
			System.out.println("l2_D_Fetch");
			System.out.println(l2_D_Fetch);
			
			System.out.println("l3_D_Fetch");
			System.out.println(l3_D_Fetch);
			
			System.out.println("l1_D_WAlloc");
			System.out.println(l1_D_WAlloc);
			
			System.out.println("l2_D_WAlloc");
			System.out.println(l2_D_WAlloc);
			
			System.out.println("l3_D_WAlloc");
			System.out.println(l3_D_WAlloc);
			
			System.out.println("l1_D_WBack");
			System.out.println(l1_D_WBack);
			
			System.out.println("l2_D_WBack");
			System.out.println(l2_D_WBack);
			
			System.out.println("l3_D_WBack");
			System.out.println(l3_D_WBack);
			
			System.out.println("l1_I_Demand_Fetches");
			System.out.println(l1_I_Demand_Fetches);
			
			System.out.println("l2_I_Demand_Fetches");
			System.out.println(l2_I_Demand_Fetches);
			
			System.out.println("l3_I_Demand_Fetches");
			System.out.println(l3_I_Demand_Fetches);
			
			System.out.println("l1_D_Demand_Fetches");
			System.out.println(l1_D_Demand_Fetches);
			
			System.out.println("l2_D_Demand_Fetches");
			System.out.println(l2_D_Demand_Fetches);
			
			System.out.println("l3_D_Demand_Fetches");
			System.out.println(l3_D_Demand_Fetches);
			
			System.out.println("l1_I_Demand_Misses");
			System.out.println(l1_I_Demand_Misses);
			
			System.out.println("l2_I_Demand_Misses");
			System.out.println(l2_I_Demand_Misses);
			
			System.out.println("l3_I_Demand_Misses");
			System.out.println(l3_I_Demand_Misses);
			
			System.out.println("l1_D_Demand_Misses");
			System.out.println(l1_D_Demand_Misses);
			
			System.out.println("l2_D_Demand_Misses");
			System.out.println(l2_D_Demand_Misses);
			
			System.out.println("l3_D_Demand_Misses");
			System.out.println(l3_D_Demand_Misses);
			
			System.out.println("l1_I_Demand_Miss_Rate");
			System.out.println(l1_I_Demand_Miss_Rate);
			
			System.out.println("l2_I_Demand_Miss_Rate");
			System.out.println(l2_I_Demand_Miss_Rate);
			
			System.out.println("l3_I_Demand_Miss_Rate");
			System.out.println(l3_I_Demand_Miss_Rate);
			
			System.out.println("l1_D_Demand_Miss_Rate");
			System.out.println(l1_D_Demand_Miss_Rate);
			
			System.out.println("l2_D_Demand_Miss_Rate");
			System.out.println(l2_D_Demand_Miss_Rate);
			
			System.out.println("l3_D_Demand_Miss_Rate");
			System.out.println(l3_D_Demand_Miss_Rate);
				
			System.out.println("l1_I_Bytes_From_Mem");
			System.out.println(l1_I_Bytes_From_Mem);
			
			System.out.println("l2_I_Bytes_From_Mem");
			System.out.println(l2_I_Bytes_From_Mem);
			
			System.out.println("l3_I_Bytes_From_Mem");
			System.out.println(l3_I_Bytes_From_Mem);
			
			System.out.println("l1_D_Bytes_From_Mem");
			System.out.println(l1_D_Bytes_From_Mem);
			
			System.out.println("l2_D_Bytes_From_Mem");
			System.out.println(l2_D_Bytes_From_Mem);
			
			System.out.println("l3_D_Bytes_From_Mem");
			System.out.println(l3_D_Bytes_From_Mem);
			
			System.out.println("l1_I_Bytes_To_Mem");
			System.out.println(l1_I_Bytes_To_Mem);
			
			System.out.println("l2_I_Bytes_To_Mem");
			System.out.println(l2_I_Bytes_To_Mem);
			
			System.out.println("l3_I_Bytes_To_Mem");
			System.out.println(l3_I_Bytes_To_Mem);
			
			System.out.println("l1_D_Bytes_To_Mem");
			System.out.println(l1_D_Bytes_To_Mem);
			
			System.out.println("l2_D_Bytes_To_Mem");
			System.out.println(l2_D_Bytes_To_Mem);
			
			System.out.println("l3_D_Bytes_To_Mem");
			System.out.println(l3_D_Bytes_To_Mem);
			 
			System.out.println("l1_I_Total_Bytes");
			System.out.println(l1_I_Total_Bytes);
			
			System.out.println("l2_I_Total_Bytes");
			System.out.println(l2_I_Total_Bytes);
			
			System.out.println("l3_I_Total_Bytes");
			System.out.println(l3_I_Total_Bytes);
			
			System.out.println("l1_D_Total_Bytes");
			System.out.println(l1_D_Total_Bytes);
			
			System.out.println("l2_D_Total_Bytes");
			System.out.println(l2_D_Total_Bytes);
			
			System.out.println("l3_D_Total_Bytes");
			System.out.println(l3_D_Total_Bytes);
		}
	
}
