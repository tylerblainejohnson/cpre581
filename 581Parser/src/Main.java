import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import parser.LineParser;


public class Main {
	private static String file = "482sphinx3.txt";
	private static String specbenchmark = "482";
	public static void main(String[] args) {
		
		long startTime = System.currentTimeMillis();
		System.out.println("Starting: " + startTime);
		LineParser parser = new LineParser(false,specbenchmark);
		try {
			Scanner fileScanner = new Scanner(new File(file));
			parser.parseStream(fileScanner);
			} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Run Time: " + (endTime - startTime)/1000 + " seconds");
	}

}
