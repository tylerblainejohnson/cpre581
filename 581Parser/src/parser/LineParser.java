package parser;

import java.util.ArrayList;
import java.util.Scanner;

import sql.SQLProject;

import containers.simulationContainer;

public class LineParser {
	//I Cache size strings
	static String l1_I_size = "-l1-isize";
	static String l2_I_size = "-l2-isize";
	static String l3_I_size = "-l3-isize";
	//D Cache size strings
	static String l1_D_size = "-l1-dsize";
	static String l2_D_size = "-l2-dsize";
	static String l3_D_size = "-l3-dsize";
	//I cache block sizes
	static String l1_I_Bsize = "-l1-ibsize";
	static String l2_I_Bsize = "-l2-ibsize";
	static String l3_I_Bsize = "-l3-ibsize";
	//I cache block sizes
	static String l1_D_Bsize = "-l1-dbsize";
	static String l2_D_Bsize = "-l2-dbsize";
	static String l3_D_Bsize = "-l3-dbsize";
	//I cache associativity
	static String l1_I_Assoc = "-l1-iassoc";
	static String l2_I_Assoc = "-l2-iassoc";
	static String l3_I_Assoc = "-l3-iassoc";
	//D cache associativity
	static String l1_D_Assoc = "-l1-dassoc";
	static String l2_D_Assoc = "-l2-dassoc";
	static String l3_D_Assoc = "-l3-dassoc";
	//I cache replacement
	static String l1_I_Repl = "-l1-irepl";
	static String l2_I_Repl = "-l2-irepl";
	static String l3_I_Repl = "-l3-irepl";
	//D cache replacement
	static String l1_D_Repl = "-l1-drepl";
	static String l2_D_Repl = "-l2-drepl";
	static String l3_D_Repl = "-l3-drepl";
	//I fetch 
	static String l1_I_Fetch = "-l1-ifetch";
	static String l2_I_Fetch = "-l2-ifetch";
	static String l3_I_Fetch = "-l3-ifetch";
	//D fetch 
	static String l1_D_Fetch = "-l1-dfetch";
	static String l2_D_Fetch = "-l2-dfetch";
	static String l3_D_Fetch = "-l3-dfetch";
	//D write allocate
	static String l1_D_WAlloc = "-l1-dwalloc";
	static String l2_D_WAlloc = "-l2-dwalloc";
	static String l3_D_WAlloc = "-l3-dwalloc";
	//D write back
	static String l1_D_WBack = "-l1-dwback";
	static String l2_D_WBack = "-l2-dwback";
	static String l3_D_WBack = "-l3-dwback";
	
	//Cache Stat seperators
	static String L1_DCache = "l1-dcache";
	static String L2_DCache = "l2-dcache";
	static String L3_DCache = "l3-dcache";
	static String L1_ICache = "l1-icache";
	static String L2_ICache = "l2-icache";
	static String L3_ICache = "l3-icache";
	
	//Run seperators
	static String Exec_Complete = "---Execution complete."; // at end of every simulation
	static String Exec_Start = "---Summary of options"; // at start of every simulation
	
	static String Demand_Fetches = "Demand Fetches";
	static String Demand_Misses = "Demand Misses";
	static String Demand_Miss_Rate = "Demand miss rate";
	static String Bytes_From_Memory = "Bytes From Memory";
	static String Bytes_To_Memory = "Bytes To Memory";
	static String Total_Bytes = "Total Bytes";
		
	private boolean debug = false;
	private SQLProject sql;
	private String specbenchmark = "";
	public LineParser(String specbenchmark){
		this.specbenchmark = specbenchmark;
		sql = new SQLProject(specbenchmark);
	}
	public LineParser(boolean debug,String specbenchmark){
		sql = new SQLProject(specbenchmark);
		this.specbenchmark = specbenchmark;
		this.debug = debug;
	}
	
	public void parseStream(Scanner scanner){
		boolean simStart = false;
		int statState = 0;
		simulationContainer simData = null;
		String line;
		String splits[];
		long lineCount = 0;
		long localLineCount = 0;
		Scanner tempScan = null;
		while(scanner.hasNextLine()){
			lineCount ++;
			localLineCount ++;
			if(localLineCount == 10000){
				System.out.println(lineCount);
				localLineCount = 0;
			}
			line = scanner.nextLine();
			if(line.contains(Exec_Start))
			{
				simStart = true;
				simData = new simulationContainer();
				if(debug){
					System.out.println("Found New Results");
				}
			}
			else if(line.contains(Exec_Complete)){
				simStart = false;
				//simData.printSimulation();
				sql.saveSimResults(simData);
				simData = null;
			}
			else if(simStart){
				if (line.contains(l1_I_size)) { // TODO
					splits = line.split(" ");
					simData.l1_I_size = Integer.parseInt(splits[1]);
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l2_I_size)) { // TODO
					splits = line.split(" ");
					simData.l2_I_size = Integer.parseInt(splits[1]);
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l3_I_size)) { // TODO
					splits = line.split(" ");
					simData.l3_I_size = Integer.parseInt(splits[1]);
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l1_D_size)) { 
					splits = line.split(" ");
					simData.l1_D_size = Integer.parseInt(splits[1]);
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l2_D_size)) { // TODO
					splits = line.split(" ");
					simData.l2_D_size = Integer.parseInt(splits[1]);
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l3_D_size)) { // TODO
					splits = line.split(" ");
					simData.l3_D_size = Integer.parseInt(splits[1]);
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l1_I_Bsize)) { // TODO
					splits = line.split(" ");
					simData.l1_I_Bsize = Integer.parseInt(splits[1]);
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l2_I_Bsize)) { // TODO
					splits = line.split(" ");
					simData.l2_I_Bsize = Integer.parseInt(splits[1]);
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l3_I_Bsize)) { // TODO
					splits = line.split(" ");
					simData.l3_I_Bsize = Integer.parseInt(splits[1]);
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l1_D_Bsize)) { // TODO
					splits = line.split(" ");
					simData.l1_D_Bsize = Integer.parseInt(splits[1]);
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l2_D_Bsize)) { // TODO
					splits = line.split(" ");
					simData.l2_D_Bsize = Integer.parseInt(splits[1]);
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l3_D_Bsize)) { // TODO
					splits = line.split(" ");
					simData.l3_D_Bsize = Integer.parseInt(splits[1]);
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l1_I_Assoc)) { // TODO
					splits = line.split(" ");
					simData.l1_I_Assoc = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l2_I_Assoc)) { // TODO
					splits = line.split(" ");
					simData.l2_I_Assoc = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l3_I_Assoc)) { // TODO
					splits = line.split(" ");
					simData.l3_I_Assoc = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l1_D_Assoc)) { // TODO
					splits = line.split(" ");
					simData.l1_D_Assoc = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l2_D_Assoc)) { // TODO
					splits = line.split(" ");
					simData.l2_D_Assoc = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l3_D_Assoc)) { // TODO
					splits = line.split(" ");
					simData.l3_D_Assoc = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l1_I_Repl)) { // TODO
					splits = line.split(" ");
					simData.l1_I_Repl = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l2_I_Repl)) { // TODO
					splits = line.split(" ");
					simData.l2_I_Repl = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if (line.contains(l3_I_Repl)) { // TODO
					splits = line.split(" ");
					simData.l3_I_Repl = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l1_D_Repl)){// TODO
					splits = line.split(" ");
					simData.l1_D_Repl = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l2_D_Repl)){// TODO
					splits = line.split(" ");
					simData.l2_D_Repl = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l3_D_Repl)){// TODO
					splits = line.split(" ");
					simData.l3_D_Repl = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l1_I_Fetch)){// TODO
					splits = line.split(" ");
					simData.l1_I_Fetch = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l2_I_Fetch)){// TODO
					splits = line.split(" ");
					simData.l2_I_Fetch = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l3_I_Fetch)){// TODO
					splits = line.split(" ");
					simData.l3_I_Fetch = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l1_D_Fetch)){// TODO
					splits = line.split(" ");
					simData.l1_D_Fetch = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l2_D_Fetch)){// TODO
					splits = line.split(" ");
					simData.l2_D_Fetch = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l3_D_Fetch)){// TODO
					splits = line.split(" ");
					simData.l3_D_Fetch = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l1_D_WAlloc)){// TODO
					splits = line.split(" ");
					simData.l1_D_WAlloc = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l2_D_WAlloc)){// TODO
					splits = line.split(" ");
					simData.l2_D_WAlloc = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l3_D_WAlloc)){// TODO
					splits = line.split(" ");
					simData.l3_D_WAlloc = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l1_D_WBack)){// TODO
					splits = line.split(" ");
					simData.l1_D_WBack = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l2_D_WBack)){// TODO
					splits = line.split(" ");
					simData.l2_D_WBack = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(l3_D_WBack)){// TODO
					splits = line.split(" ");
					simData.l3_D_WBack = splits[1];
					if(debug){
						System.out.println(splits[0]);
						System.out.println(splits[1]);	
					}
				} else if(line.contains(L1_DCache)){// TODO
					statState = 1;
					if(debug){
						System.out.println("Gathering L1 D Cache Stats");
					}
				} else if(line.contains(L2_DCache)){// TODO
					statState = 2;
					if(debug){
						System.out.println("Gathering L2 D Cache Stats");
					}
				} else if(line.contains(L3_DCache)){// TODO
					statState = 3;
					if(debug){
						System.out.println("Gathering L3 D Cache Stats");
					}
				} else if(line.contains(L1_ICache)){// TODO
					statState = 4;
					if(debug){
						System.out.println("Gathering L1 I Cache Stats");
					}
				} else if(line.contains(L2_ICache)){// TODO
					statState = 5;
					if(debug){
						System.out.println("Gathering L2 I Cache Stats");
					}
				} else if(line.contains(L3_ICache)){// TODO
					statState = 6;
					if(debug){
						System.out.println("Gathering L3 I Cache Stats");
					}
				} else if(line.contains(Demand_Misses)){
					tempScan = new Scanner(line).useDelimiter("\\s+");
					while(!tempScan.hasNextInt() && tempScan.hasNext()){
						tempScan.next();
					}
					int misses = tempScan.nextInt();
					tempScan.close();
					tempScan = null;
					switch(statState){
						case 0: break;
						case 1:
							simData.l1_D_Demand_Misses = misses;
							break;
						case 2:
							simData.l2_D_Demand_Misses = misses;
							break;
						case 3:
							simData.l3_D_Demand_Misses = misses;
							break;
						case 4:
							simData.l1_I_Demand_Misses = misses;
							break;
						case 5:
							simData.l2_I_Demand_Misses = misses;
							break;
						case 6:
							simData.l3_I_Demand_Misses = misses;
							break;
					}
				} else if(line.contains(Demand_Miss_Rate)){
					tempScan = new Scanner(line).useDelimiter("\\s+");
					while(!tempScan.hasNextDouble() && tempScan.hasNext()){
						tempScan.next();
					}
					double missRate = tempScan.nextDouble();
					tempScan.close();
					tempScan = null;
					switch(statState){
					case 0: break;
					case 1:
						simData.l1_D_Demand_Miss_Rate = missRate;
						break;
					case 2:
						simData.l2_D_Demand_Miss_Rate = missRate;
						break;
					case 3:
						simData.l3_D_Demand_Miss_Rate = missRate;
						break;
					case 4:
						simData.l1_I_Demand_Miss_Rate = missRate;
						break;
					case 5:
						simData.l2_I_Demand_Miss_Rate = missRate;
						break;
					case 6:
						simData.l3_I_Demand_Miss_Rate = missRate;
						break;
				}
				} else if(line.contains(Bytes_From_Memory)){
					tempScan = new Scanner(line).useDelimiter("\\s+");
					while(!tempScan.hasNextInt() && tempScan.hasNext()){
						tempScan.next();
					}
					int bytes = tempScan.nextInt();
					tempScan.close();
					tempScan = null;
					switch(statState){
					case 0: break;
					case 1:
						simData.l1_D_Bytes_From_Mem = bytes;
						break;
					case 2:
						simData.l2_D_Bytes_From_Mem = bytes;
						break;
					case 3:
						simData.l3_D_Bytes_From_Mem = bytes;
						break;
					case 4:
						simData.l1_I_Bytes_From_Mem = bytes;
						break;
					case 5:
						simData.l2_I_Bytes_From_Mem = bytes;
						break;
					case 6:
						simData.l3_I_Bytes_From_Mem = bytes;
						break;
				}
				} else if(line.contains(Bytes_To_Memory)){
					tempScan = new Scanner(line).useDelimiter("\\s+");
					while(!tempScan.hasNextInt() && tempScan.hasNext()){
						tempScan.next();
					}
					int bytes = tempScan.nextInt();
					tempScan.close();
					tempScan = null;
					switch(statState){
					case 0: break;
					case 1:
						simData.l1_D_Bytes_To_Mem = bytes;
						break;
					case 2:
						simData.l2_D_Bytes_To_Mem = bytes;
						break;
					case 3:
						simData.l3_D_Bytes_To_Mem = bytes;
						break;
					case 4:
						simData.l1_I_Bytes_To_Mem = bytes;
						break;
					case 5:
						simData.l2_I_Bytes_To_Mem = bytes;
						break;
					case 6:
						simData.l3_I_Bytes_To_Mem = bytes;
						break;
				}
					
				} else if(line.contains(Total_Bytes)){
					tempScan = new Scanner(line).useDelimiter("\\s+");
					while(!tempScan.hasNextInt() && tempScan.hasNext()){
						tempScan.next();
					}
					int bytes = tempScan.nextInt();
					tempScan.close();
					tempScan = null;
					switch(statState){
					case 0: break;
					case 1:
						simData.l1_D_Total_Bytes = bytes;
						break;
					case 2:
						simData.l2_D_Total_Bytes = bytes;
						break;
					case 3:
						simData.l3_D_Total_Bytes = bytes;
						break;
					case 4:
						simData.l1_I_Total_Bytes = bytes;
						break;
					case 5:
						simData.l2_I_Total_Bytes = bytes;
						break;
					case 6:
						simData.l3_I_Total_Bytes = bytes;
						break;
				}
				}
			}
		}
	}
	
}
